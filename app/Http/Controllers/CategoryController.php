<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class CategoryController extends Controller
{

    public function index()
    {     
        $category = Auth::user()->categories()->latest()->paginate(8);
        
        return view('category.category',compact('category'));
    }

    public function create()
    {
        $category = Category::all();
        return view('category.addcategory',compact('category'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $category = new Category();
        $category->user_id = Auth::id();
        $category->name = $request->name;
        $category->save();
        return redirect()->route('category');
    }


    public function edit($cat_id)
    {
        $category = Category::find($cat_id);
        return view('category.editcategory',compact('category'));
     
    }


    public function update(Request $request, $cat_id)
    {
        
        $this->validate($request, [
            'name' => 'required'
        ]);
        $category = Category::find($cat_id);
        $category->name = $request->name;
        $category->save();   
        return redirect()->route('category');
    }


    public function destroy($cat_id)
    {
        $category = Category::find($cat_id);
        $category->delete();
        return redirect()->back();
    }
}
