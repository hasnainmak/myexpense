<?php

namespace App\Http\Controllers;

use App\Expense;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExpenseController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
     
        $expensesjoin = DB::table('expenses')
        ->leftJoin('categories', 'expenses.exp_id', '=', 'categories.cat_id')
        ->where('expenses.user_id', '=', Auth::id())
        ->orderby('expenses.created_at','desc')
        ->paginate(8);
        
  
        return view('expense.expense',compact('expensesjoin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $expense = Auth::user()->categories()->latest()->get();
   
        return view('expense.addexpense',compact('expense'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      

        $this->validate($request, [
            'amount' => 'required|max:5000000',
            'entry_date' => 'required|date'
        ]);
        $expense = new Expense();
        $expense->user_id = Auth::id();
        $expense->exp_id = $request->expenses;
        $expense->amount = $request->amount;
        $expense->entry_date = $request->entry_date;
        $expense->save();
        
        $expense->categories()->attach($request->categories);
        return redirect()->route('expense');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expense::find($id);
        return view('expense.editexpense',compact('expense'));
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'amount' => 'required|max:5000000',
            'entry_date' => 'required|date'
        ]);

     
      
        $expense = Expense::find($id);
        $expense->exp_id = $request->expenses;
        $expense->amount = $request->amount;
        $expense->entry_date = $request->entry_date;
        $expense->save();
        $expense->categories()->attach($request->categories);
       
        return redirect()->route('expense');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = Expense::find($id);
        $expense->delete();

        return redirect()->route('expense');
    }
}
