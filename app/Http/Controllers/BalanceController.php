<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Balance;

class BalanceController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $balance =  Auth::user()->balances()->latest()->paginate(8);
        return view('balance.balance',compact('balance'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $balance = Balance::all();
        return view('balance.addbalance',compact('balance'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|max:5000000',
            'entry_date' => 'required|date'
        ]);
        $balance = new balance();
        $balance->user_id = Auth::id();
        $balance->amount = $request->amount;
        $balance->entry_date = $request->entry_date;
        $balance->save();
        return redirect()->route('balance');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $balance = balance::find($id);
        return view('balance.editbalance',compact('balance'));
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'amount' => 'required|max:5000000',
            'entry_date' => 'required|date'
        ]);

     
      
        $balance = balance::find($id);
        $balance->amount = $request->amount;
        $balance->entry_date = $request->entry_date;
        $balance->save();
       
        return redirect()->route('balance');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $balance = balance::find($id);
        $balance->delete();

        return redirect()->back();
    }
}
