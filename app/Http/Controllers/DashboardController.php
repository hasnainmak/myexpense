<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Balance;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index()
    {

        $from = Carbon::parse(sprintf(
            '%s-%s-01',
            request()->query('y', Carbon::now()->year),
            request()->query('m', Carbon::now()->month)
        ));
        $to      = clone $from;
        $to->day = $to->daysInMonth;
      //  $category = Auth::user()->categories()->latest()->get()


   
        $expenses = DB::table('expenses')
        ->leftJoin('categories', 'expenses.exp_id', '=', 'categories.cat_id')->where('expenses.user_id', '=', Auth::id())
        ->whereBetween('entry_date', [$from, $to]);
       
       
    
        $balances = Auth::user()->balances()->whereBetween('entry_date', [$from, $to]);
        $expensesTotal   = $expenses->sum('amount');
        $balanceTotal    = $balances->sum('amount');
        $groupedExpenses = $expenses->whereNotNull('exp_id')->orderBy('amount', 'desc')->get()->groupBy('exp_id');
       // dd($groupedExpenses);

        $profit          = $balanceTotal - $expensesTotal;
        $expensesSummary = [];

        foreach ($groupedExpenses as $exp) {
            foreach ($exp as $line) {
                if (!isset($expensesSummary[$line->name])) {
                    $expensesSummary[$line->name] = [
                        'name'   => $line->name,
                        'amount' => 0,
                    ];
                }
                $expensesSummary[$line->name]['amount'] += $line->amount;
            }  
        }

     
      //dd($expensesSummary);

        return view('dashboard', compact(
            'expensesSummary',
            'expensesTotal',
            'balanceTotal',
            'profit'
        ));
    }
}
