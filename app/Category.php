<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'cat_id';
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function expenses(){
        return $this->belongsToMany(Expense::class)->withTimestamps();
    }

    
}
