<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'DashboardController@index')->name('home')->middleware('auth');

Route::get('/category', 'CategoryController@index')->name('category')->middleware('auth');
Route::get('/addcategory', 'CategoryController@create')->name('addcategory')->middleware('auth');
Route::post('/category','CategoryController@store')->name('categorystore')->middleware('auth');
Route::patch('/category/{cat_id}', 'CategoryController@update')->name('categoryupdate')->middleware('auth');
Route::get('/category/{cat_id}/edit', 'CategoryController@edit')->name('editcategory')->middleware('auth');
Route::delete('/category/{cat_id}', 'CategoryController@destroy')->name('categorydestroy')->middleware('auth');



Route::get('/balance', 'BalanceController@index')->name('balance')->middleware('auth');
Route::get('/addbalance', 'BalanceController@create')->name('addbalance')->middleware('auth');
Route::post('/balance','BalanceController@store')->name('balancestore')->middleware('auth');
Route::patch('/balance/{id}', 'BalanceController@update')->name('balanceupdate')->middleware('auth');
Route::get('/balance/{id}/edit', 'BalanceController@edit')->name('editbalance')->middleware('auth');
Route::delete('/balance/{id}', 'BalanceController@destroy')->name('balancedestroy')->middleware('auth');



Route::get('/expense', 'ExpenseController@index')->name('expense')->middleware('auth');
Route::get('/addexpense', 'ExpenseController@create')->name('addexpense')->middleware('auth');
Route::post('/expense','ExpenseController@store')->name('expensestore')->middleware('auth');
Route::patch('/expense/{id}', 'ExpenseController@update')->name('expenseupdate')->middleware('auth');
Route::get('/expense/{id}/edit', 'ExpenseController@edit')->name('editexpense')->middleware('auth');
Route::delete('/expense/{id}', 'ExpenseController@destroy')->name('expensedestroy')->middleware('auth');


