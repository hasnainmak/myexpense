@extends('layouts.app')

@section('title')
Report Dashboard
@endsection

@section('content')
<div class="content">
    <div class="row">
      
          
        @foreach ($expensesSummary as $expenses)
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
             
              </div>
              <div class="col-7 col-md-8">
               
                <div class="numbers">
                <p class="card-category">{{ $expenses['name'] }}</p>
                  <p class="card-title">{{$expenses['amount']}}
                  </p>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach
   
   
   <div class="col-md-12">
      <div class="card card-stats">
          <div class="card-body ">
              <div class="numbers">
              <p class="card-category">Total Balance</p>
              <p class="card-title">{{$balanceTotal}}</p>
              </div>

              <div class="numbers">
                  <p class="card-category">Expenses Total</p>
                  <p class="card-title">{{$expensesTotal}}</p>
                  </div>

              <div class="numbers">
                  <p class="card-category">Current Balance</p>
                  <p class="card-title">{{$profit}}</p>
                  </div>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection
