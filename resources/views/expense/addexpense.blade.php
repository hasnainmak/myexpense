@extends('layouts.app')

@section('title')
Add Expense
@endsection

@section('content')

<form action="{{ route('expensestore') }}" method="post" enctype="multipart/form-data">
    @csrf

    <div class="form-group form-float">
        <div class="form-line {{ $errors->has('expenses')? 'focused error' : '' }}">
           
            
            <label class="form-label">Category</label>
            <select name="expenses"  class="form-control d-flex">

                @foreach($expense as $expenses)
                    <option value="{{ $expenses->cat_id }}">
                        {{ $expenses->name }}
                    </option>
                    @endforeach
            </select>
        </div>
    </div>

    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Entry Date</label>
            <input type="date" id="entry_date" class="form-control" name="entry_date">
            
        </div>
    </div>

    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Amount</label>
            <input type="number" min="0.01" step="0.01" max="100000000" value="0.00" id="amount" class="form-control" name="amount">
            
        </div>
    </div>



    <a class="btn btn-primary btn-round " href="{{ route('expense') }}">BACK</a>
    <button type="submit" class="btn btn-danger btn-round ">
    ADD</button>
</form>

@endsection
