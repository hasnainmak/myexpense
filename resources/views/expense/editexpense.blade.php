@extends('layouts.app')

@section('title')
Edit Expense
@endsection

@section('content')

<form action="{{ route('expenseupdate',$expense->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')

    <div class="form-group form-float">
            <div class="form-line {{ $errors->has('expenses')? 'focused error' : '' }}">
               
                
                <label class="form-label">Category</label>
                <select name="expenses"  class="form-control d-flex">
                    
                        
                        <option value="{{ $expense->exp_id }}">
                            {{ $expense->exp_id }}
                        </option>
                     
                    
          
                </select>
            </div>
        </div>

    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Entry Date</label>
            <input type="date" data-date="true"
            value="{{ $expense->entry_date }}"  id="entry_date" class="form-control" name="entry_date">
            
        </div>
    </div>

    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Amount</label>
            <input type="number" min="0.01" step="0.01" max="10000000" value="{{ $expense->amount }}" id="amount" class="form-control" name="amount">
            
        </div>
    </div>



    <a class="btn btn-primary btn-round " href="{{ route('expense') }}">BACK</a>
    <button type="submit" class="btn btn-danger btn-round ">
    UPDATE</button>
</form>

@endsection
