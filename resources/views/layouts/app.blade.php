<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8" />
  <title>myExpense Manager</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}"> 
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{ asset('assets/css/paper-dashboard.css?v=2.0.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href=" {{ asset('assets/demo/demo.css')}} " rel="stylesheet" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
  @stack('css')
  <script src="{{ asset('assets/js/core/jquery.min.js')}}"></script>
      
</head>

<body class="">

  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="./assets/img/logo-small.png">
          </div>
        </a>
        <a href="{{ route('home') }}" class="simple-text logo-normal">
          myExpense
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav" id="nav">
         
            
            @if (Request::is(''))
                
            @else
              
          <li class="{{ Request::is('/')? 'active' : '' }}">
            <a href=" {{ route('home') }} ">
              <i class="material-icons">
                account_balance
                </i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="{{ Request::is('category','addcategory','editcategory')? 'active' : '' }}">
          <a href=" {{ route('category') }}">
            <i class="material-icons">
              category
              </i>
              <p>Categories</p>
            </a>
          </li>
          <li class="{{ Request::is('expense','addexpense','editexpense')? 'active' : '' }}">
            <a href=" {{ route('expense') }}">
              <i class="material-icons">
                account_balance_wallet
                </i>
              <p>Expenses</p>
            </a>
          </li>
          <li class="{{ Request::is('balance','addbalance','editbalance')? 'active' : '' }}">
              <a href=" {{ route('balance') }}">
                <i class="material-icons">
                  euro_symbol
                  </i>
              <p>Balance</p>
            </a>
          </li>
          <li>
        
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
             <i class="material-icons">
              person
              </i>
              <p>Logout</p>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </a>
        
          </li>
          @endif
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="">  @yield('title')</a>
          </div>

          <div class="collapse navbar-collapse justify-content-end" id="navigation">
        
            <ul class="navbar-nav">
       
      
            
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
   
          
  

  <!--   content here   -->
  <div class="content">
    <div class="row">
      <div class="col-md-12">
      @yield('content')
      </div>
    </div>
  </div>
  <!--  end of content   -->

    <!--  footer   -->
  <footer class="footer footer-black  footer-white ">
    <div class="container-fluid">
      <div class="row">
          <span class="copyright" style="padding-left:40px">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script>. designed by hsncreative
          </span>
        </div>
    </div>
  </footer>
  <!--  endof footer   -->

</div>
</div>
 
  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/popper.min.js')}}"></script>
  <script src="{{ asset('assets/js/core/bootstrap.min.js')}}"></script>
  <script src="{{ asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>

 
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('assets/js/paper-dashboard.min.js?v=2.0.0')}}" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  
  @stack('js')
</body>

</html>
