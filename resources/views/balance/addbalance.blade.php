@extends('layouts.app')

@section('title')
Add Balance
@endsection

@section('content')

<form action="{{ route('balancestore') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Entry Date</label>
            <input type="date" data-date="true"  id="entry_date" class="form-control" name="entry_date">
            
        </div>
    </div>

    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Amount</label>
            <input type="number" min="0.01" step="0.01" max="100000000" value="0.00" id="amount" class="form-control" name="amount">
            
        </div>
    </div>



    <a class="btn btn-primary btn-round " href="{{ route('balance') }}">BACK</a>
    <button type="submit" class="btn btn-danger btn-round ">
    ADD</button>
</form>

@endsection
