@extends('layouts.app')

@section('title')
Balance
@endsection

@section('content')



    <div class="col-md-12">
    <a href="{{ route('addbalance')}}">
                <button class="btn btn-danger btn-round d-flex" style="margin-top:-10px;margin-bottom:20px">
                    <i class="material-icons"> add_circle</i>
                    <div style="padding-top:5px;padding-left:10px">Add Balance </div></button>
                </a>
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">All Balance</h4>
              </div>
              <div class="card-body">
                  <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                        Entry Date
                      </th>
                      <th>
                          Amount
                        </th>
                      <th>
                       Actions
                      </th>
                     
                    </thead>

                    @foreach($balance as $key=>$balances)
                    <tbody>
                      <tr>
                        <td>
                           {{$balances->entry_date}}
                        </td>

                        <td>
                            {{$balances->amount}}
                         </td>
                        <td class="d-flex">
                        <a href="{{ route('editbalance',$balances->id)}}"><button class="btn btn-danger btn-round btn-sm" type="button">
                            <i class="material-icons" style="font-size: 18px;">edit</i>
                        </button></a>
                       
                        <form action="{{ route('balancedestroy',$balances->id) }}"
                            method="POST" style="display:inline-block">
                        @csrf
                        @method('DELETE')
                         <button class="btn btn-danger btn-round btn-sm" style="margin-left:10px" type="submit">
                            <i class="material-icons" style="font-size: 18px;">delete</i>
                        </button></a>
                      </form>

                        </td>
                      </tr>
                      @endforeach
                      <span>{{$balance->links()}}</span>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
@endsection