@extends('layouts.app')

@section('title')
Edit Balance
@endsection

@section('content')

<form action="{{ route('balanceupdate',$balance->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Entry Date</label>
            <input type="date" data-date="true"
            value="{{ $balance->entry_date }}"  id="entry_date" class="form-control" name="entry_date">
            
        </div>
    </div>

    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Amount</label>
            <input type="number" min="0.01" step="0.01" max="10000000" value="{{ $balance->amount }}" id="amount" class="form-control" name="amount">
            
        </div>
    </div>



    <a class="btn btn-primary btn-round " href="{{ route('balance') }}">BACK</a>
    <button type="submit" class="btn btn-danger btn-round ">
    UPDATE</button>
</form>

@endsection
