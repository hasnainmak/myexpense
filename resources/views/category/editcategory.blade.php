@extends('layouts.app')

@section('title')
Edit Category
@endsection

@section('content')

<form action="{{ route('categoryupdate',$category->cat_id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Category Name</label>
            <input type="text" id="name" class="form-control" name="name"
            value="{{ $category->name }}">
            
        </div>
    </div>



    <a class="btn btn-primary btn-round " href="{{ route('category') }}">BACK</a>
    <button type="submit" class="btn btn-danger btn-round ">
    UPDATE</button>
</form>

@endsection
