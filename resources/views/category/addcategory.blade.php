@extends('layouts.app')

@section('title')
Add Category
@endsection

@section('content')

<form action="{{ route('categorystore') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Category Name</label>
            <input type="text" id="name" class="form-control" name="name">
            
        </div>
    </div>



    <a class="btn btn-primary btn-round " href="{{ route('category') }}">BACK</a>
    <button type="submit" class="btn btn-danger btn-round ">
    ADD</button>
</form>

@endsection
