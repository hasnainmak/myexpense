@extends('layouts.app')

@section('title')
Category
@endsection

@section('content')



    <div class="col-md-12">
    <a href="{{ route('addcategory')}}">
                <button class="btn btn-danger btn-round d-flex" style="margin-top:-10px;margin-bottom:20px">
                    <i class="material-icons"> add_circle</i>
                    <div style="padding-top:5px;padding-left:10px">Add Category </div></button>
                </a>
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Categories</h4>
              </div>
              <div class="card-body">
                  <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                        Name
                      </th>
                      <th>
                       Actions
                      </th>
                     
                    </thead>

                    @foreach($category as $key=>$categories)
                    <tbody>
                      <tr>
                        <td>
                           {{$categories->name}}
                        </td>
                        <td class="d-flex">
                        <a href="{{ route('editcategory',$categories->cat_id)}}"><button class="btn btn-danger btn-round btn-sm" type="button">
                            <i class="material-icons " style="font-size: 18px;">edit</i>
                        </button></a>
                       
                        <form action="{{ route('categorydestroy',$categories->cat_id) }}"
                            method="POST" style="display:inline-block">
                        @csrf
                        @method('DELETE')
                         <button class="btn btn-danger btn-round btn-sm" style="margin-left:10px" type="submit">
                            <i class="material-icons" style="font-size: 18px;">delete</i>
                        </button></a>
                      </form>

                        </td>
                      </tr>
                      @endforeach
                      <span>{{$category->links()}}</span>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
    </div>
       
@endsection